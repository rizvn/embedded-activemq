package com.rizvn.embedded_activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Message Producer
 * Created by Riz
 */
public class Producer {
  MessageProducer producer;
  Session session;
  Connection connection;

  public void sendTextMessage(String aMessage){
    try {
      // Create a messages
      TextMessage message = session.createTextMessage(aMessage);

      // Tell the producer to send the message
      System.out.println("Sent message: " + aMessage);
      producer.send(message);
    }
    catch (Exception ex){
      throw new RuntimeException(aMessage);
    }
  }

  /**
   * @param aBrokerUri Uri to the broker
   * @param aQueue Queue send message
   */
  public Producer(String aBrokerUri, String aQueue){
    try {
      ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUri);
      connection = connectionFactory.createConnection();
      connection.start();

      session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
      Destination destination = session.createQueue(aQueue);

      producer = session.createProducer(destination);
      producer.setDeliveryMode(DeliveryMode.PERSISTENT);

    }
    catch (Exception e) {
      System.out.println("Caught: " + e);
      e.printStackTrace();
    }
  }

  public void close(){
    try {
      session.close();
      connection.close();
    }
    catch (Exception ex)
    {
      throw new RuntimeException(ex);
    }
  }
}