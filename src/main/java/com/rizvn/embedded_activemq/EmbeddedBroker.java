package com.rizvn.embedded_activemq;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.store.kahadb.KahaDBPersistenceAdapter;

import java.io.File;

/**
 * Created by Riz
 */
public class EmbeddedBroker {


  String brokerUri;
  String dataDir = "kaha";
  final BrokerService broker = new BrokerService();

  /**
   * Embedded broker
   * @param aBrokerUri broker urei
   */
  public EmbeddedBroker(String aBrokerUri)
  {
    brokerUri = aBrokerUri;
    start();
  }

  /**
   * Embedded broker
   * @param aBrokerUri broker urei
   * @param aDataDir directory for persistenc
   */
  public EmbeddedBroker(String aBrokerUri, String aDataDir)
  {
    dataDir = aDataDir;
    brokerUri = aBrokerUri;
    start();
  }

  public void start(){
    try {
      broker.setBrokerName("test");
      broker.addConnector(brokerUri);
      broker.setUseJmx(false);
      broker.setPersistent(true);

      KahaDBPersistenceAdapter persistenceAdapter = new KahaDBPersistenceAdapter();
      persistenceAdapter.setDirectory(new File(dataDir));
      broker.setPersistenceAdapter(persistenceAdapter);
      broker.start();
      broker.waitUntilStarted();
    }
    catch (Exception ex){
      throw new RuntimeException(ex);
    }
  }

  public void stop(){
    try {
      broker.stop();
    }
    catch (Exception ex){
      throw new RuntimeException(ex);
    }
  }
}
