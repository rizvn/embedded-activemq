package com.rizvn.embedded_activemq;

import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.*;

/**
 * Message consumer
 * Created by Riz
 */
public class Consumer implements MessageListener {
  MessageConsumer mConsumer;
  Session mSession;
  Connection mConnection;

  /**
   * @param brokerUri Uri for the broker
   * @param queue Queue from which to consume message
   */
  public Consumer(String brokerUri, String queue){
    try {
      ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUri);
      mConnection = connectionFactory.createConnection();
      mConnection.start();

      mSession = mConnection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
      Destination destination = mSession.createQueue(queue);
      mConsumer =  mSession.createConsumer(destination);
      mConsumer.setMessageListener(this);
    }
    catch (Exception e) {
      System.out.println("Caught: " + e);
      e.printStackTrace();
    }
  }

  /**
   * Close consumer
   */
  public void close(){
    try {
      mConsumer.close();
      mSession.close();
      mConnection.close();
    }
    catch (Exception aEx){
      throw new RuntimeException(aEx);
    }
  }

  /**
   * Called when an expection occurs
   * @param ex
   */
  public synchronized void onException(JMSException ex) {
    ex.printStackTrace();
    System.out.println("JMS Exception occured.  Shutting down client.");
  }

  /**
   * Called when a message is received
   * @param aMessage the message
   */
  @Override
  public void onMessage(Message aMessage) {
    try {
      if (aMessage instanceof TextMessage) {
        TextMessage textMessage = (TextMessage) aMessage;
        String text = textMessage.getText();
        System.out.println("Received: " + text);
        //acknowledge that the message has been recieved, so it remove from queue
         aMessage.acknowledge();
      } else {
        System.out.println("Received: " + aMessage);
      }
    }
    catch (Exception ex)
    {
      throw new RuntimeException(ex);
    }
  }
}